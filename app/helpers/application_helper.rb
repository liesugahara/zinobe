module ApplicationHelper
  def notification_success(notification)
    (200..299).include?(notification) ? true : false
  end
end
