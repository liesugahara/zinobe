class SendTextMessageJob < ApplicationJob
  include ApplicationHelper

  queue_as :default

  def perform(phone_number)
    body = {    
      :to => "#{phone_number}",
      :message => "Su registro fue realizado satisfactoriamente"
    }.to_json
    @result = HTTParty.post('http://localhost:3004/messages', 
    :body => body, 
    :headers => { 'Content-Type' => 'application/json' })
    notified = notification_success(@result.code)
    @hash = {
      notified: notified,
      code: @result.code,
      response: @result.parsed_response
    }
    save_response(phone_number)
  end

  def save_response(phone_number)
    User.find_by_phone_number(phone_number).update(sms_status: @hash)
  end
end
