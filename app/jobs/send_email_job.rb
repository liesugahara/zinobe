class SendEmailJob < ApplicationJob
  include ApplicationHelper

  queue_as :default

  def perform(email)
    body = {    
      :to => "#{email}",
      :message => 'Su registro fue realizado satisfactoriamente',
      :subject => 'Registro exitoso'
    }.to_json
    @result = HTTParty.post('http://localhost:3004/emails', 
    :body => body, 
    :headers => { 'Content-Type' => 'application/json' })
    notified = notification_success(@result.code)
    @hash = {
      notified: notified,
      code: @result.code,
      response: @result.parsed_response
    }
    save_response(email)
  end

  def save_response(email)
    User.find_by_email(email).update(email_status: @hash)
  end

end
