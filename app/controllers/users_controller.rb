class UsersController < ApplicationController

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url, notice: 'Thank you for signing up!'
    else
      render 'new'
    end

    rescue ActiveRecord::RecordNotUnique
      flash.now[:alert] = 'El usuario ya existe en el sistema'
      render 'new'
  end

  def index
    return nil if params[:search].nil?
    @users = User.search(params[:search])
  end

  def show
  end

  private
  def user_params
    up = params.require(:user).permit(:name, :password, :password_confirmation, :email, :identification, :phone_number, :role, :search)
    up[:role] = up[:role].to_i
    up
  end

end