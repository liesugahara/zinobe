class User < ApplicationRecord
  include ApplicationHelper

  has_secure_password
  PASSWORD_REQUIREMENTS = /\A
  (?=.{8,})
  (?=.*\d)
  (?=.*[a-z])
  (?=.*[A-Z])
  (?=.*[[:^alnum:]])
  /x
  ROLES ||= { beneficiario: 0, padrino: 1 }.freeze
  enum role: ROLES

  validates :name, length: {minimum: 3}
  validates :email, format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }
  validates :identification, uniqueness: true
  validates :phone_number, uniqueness: true
  validates :password, format: PASSWORD_REQUIREMENTS
  serialize :email_status
  serialize :sms_status

  after_create :send_notification, :update_notification_status

  def self.search(param)
    User.where('name LIKE :input OR email LIKE :input', input: "%#{param}%")
  end

  def send_notification
    SendTextMessageJob.set(wait_until: 1.minutes.from_now).perform_later(phone_number) if beneficiario?
    SendEmailJob.set(wait_until: 1.minutes.from_now).perform_later(email) if padrino?
  end

  def notification_message
    return "No se ha enviado la notificación" if sms_status.nil? && email_status.nil?
    return "No se ha enviado el sms" if sms_status && sms_status[:code].nil? && beneficiario?
    return "No se ha enviado el email" if email_status && email_status[:code].nil? && padrino?
    return "El sms ha sido enviado" if sms_status && sms_status[:notified] && beneficiario?
    return "El sms ha sido enviado" if email_status && email_status[:notified] && padrino?
    return "El mensaje se ha enviado satisfactoriamente" if sms_status && email_status && sms_status[:notified] && email_status[:notified]
    return "Hubo un error al enviar el mensaje"
  end

  def status_code
    return "" if sms_status.nil? && email_status.nil?
    return sms_status[:code] if beneficiario? && sms_status[:notified] 
    return email_status[:code] if padrino? && email_status[:notified]
    return ""
  end

  def update_notification_status
    hash = {
      notified: false,
      code: nil,
      response: "Sent"
    }
    update(sms_status: hash) if beneficiario?
    update(email_status: hash) if padrino?
  end
end
