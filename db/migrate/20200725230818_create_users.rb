class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :password_digest, null: false
      t.string :phone_number, null: false
      t.string :identification, null: false
      t.integer :role, null: false
      t.text :sms_status
      t.text :email_status
      t.timestamps
    end
    add_index :users, :name
    add_index :users, :email, unique: true
  end
end
