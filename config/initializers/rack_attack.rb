class Rack::Attack

  SIGN_IN_PATHS ||= %w(/login /sessions/new /sessions).freeze

  ### Configure Cache ###

  # If you don't want to use Rails.cache (Rack::Attack's default), then
  # configure it here.
  #
  # Note: The store is only used for throttling (not blacklisting and
  # whitelisting). It must implement .increment and .write like
  # ActiveSupport::Cache::Store

  # Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new

  ### Throttle Spammy Clients ###

  # If any single client IP is making tons of requests, then they're
  # probably malicious or a poorly-configured scraper. Either way, they
  # don't deserve to hog all of the app server's CPU. Cut them off!
  #
  # Note: If you're serving assets through rack, those requests may be
  # counted by rack-attack and this throttle may be activated too
  # quickly. If so, enable the condition to exclude them from tracking.

  # Throttle all requests by IP (60rpm)
  #
  # Key: "rack::attack:#{Time.current.to_i/:period}:req/ip:#{req.ip}"
  # throttle('req/ip', limit: 30, period: 5.seconds) do |req|
  #   req.ip unless req.path.start_with?('/assets')
  # end

  ### Prevent Brute-Force Login Attacks ###

  # The most common brute-force login attack is a brute-force password
  # attack where an attacker simply tries a large number of emails and
  # passwords to see if any credentials match.
  #
  # Another common method of attack is to use a swarm of computers with
  # different IPs to try brute-forcing a password for a specific account.

  # Throttle POST requests to /sign_in by IP address
  #
  # Key: "rack::attack:#{Time.current.to_i/:period}:sign_in/ip:#{req.ip}"
  throttle('sign_in/ip', limit: 5, period: 60.seconds) do |req|
    req.ip if SIGN_IN_PATHS.include?(req.path) && req.post?
  end

  # Throttle POST requests to /sign_in by email param
  #
  # Key: "rack::attack:#{Time.current.to_i/:period}:sign_in/email:#{req.email}"
  #
  # Note: This creates a problem where a malicious user could intentionally
  # throttle logins for another user and force their login requests to be
  # denied, but that's not very common and shouldn't happen to you. (Knock
  # on wood!)
  throttle('sign_in/email', limit: 5, period: 60.seconds) do |req|
    req.params['email'].presence if SIGN_IN_PATHS.include?(req.path) && req.post?
  end

  ### Custom Throttle Response ###

  # By default, Rack::Attack returns an HTTP 429 for throttled responses,
  # which is just fine.
  #
  # If you want to return 503 so that the attacker might be fooled into
  # believing that they've successfully broken your app (or you just want to
  # customize the response), then uncomment these lines.
  # self.throttled_response = lambda do |env|
  #  [ 503,  # status
  #    {},   # headers
  #    ['']] # body
  # end
end



# class Rack::Attack
# #   Rack::Attack.throttle("logins/ip", limit: 3, period: 15.minutes) do |req|
# #     req.params['email'] if req.path == '/sessions' && req.post?
# #   end

# #   Rack::Attack.throttle("req/ip", limit: 3, period: 15.minutes) do |req|
# #     if req.path == '/sessions/new' && req.post?
# #       # return the email if present, nil otherwise
# #       req.params['email'].presence
# #     end
# #  end
# Rack::Attack.cache.store = ActiveSupport::Cache::MemoryStore.new 

#   throttle("logins/ip", limit: 3, period: 15.minutes) do |req|
#     req.ip
#   end

#   throttle("limit logins per email", limit: 3, period: 60.seconds) do |req|
#     if req.path == '/sessions' && req.post?
#       req.params['email'].presence
#     end
#   end
# # throttle("limit logins per email", limit: 3, period: 60.seconds) do |req|
# #   if req.path == "/sessions/new" && req.post?
# #     if (req.params["name"].to_s.size > 0) and (req.params["name"]["email"].to_s.size > 0)
# #       req.params["name"]["email"]
# #     end
# #   end
# # end

# end