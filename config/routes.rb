Rails.application.routes.draw do
  root 'users#index'
  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  get 'signup', to: 'users#new', as: 'signup'
  post 'signup', to: 'users#create'
  get 'login', to: 'sessions#new', as: 'login'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get '*unmatched_route', to: 'application#not_found'
  get '/', to: 'users#index'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
